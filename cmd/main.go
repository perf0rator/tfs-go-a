package main

import (
	//"database/sql"
	"log"
	"net/http"
	handlers "tfs-go-a/cmd/auth-api"
	"tfs-go-a/internal/lot"
	"tfs-go-a/internal/user"

	"github.com/go-chi/chi"
	_ "github.com/jinzhu/gorm/dialects/postgres"
)

func main() {

	user.InitialUserMigration()
	lot.InitialLotMigration()

	//sessions := make(map[string]session.Session)

	r := chi.NewRouter()
	r.Route("/api/v1", func(r chi.Router) {
		r.Post("/signup", handlers.SignUpHandler)
		r.Post("/signin", handlers.SignInHandler)
		r.Put("/users/{userID}", handlers.UpdateUser)
		r.Get("/users/{userID}/lots", handlers.UserLotsHandler)
		r.Delete("/users/{userID}", handlers.DeleteUserHandler)
		r.Post("/lots", handlers.CreateLotHandler)
		r.Get("/lots/{lotID}", handlers.GetLotHandler)
		r.Patch("/users/{userID}/lot/{lotID}", handlers.UpdateLotHandler)
		r.Delete("/lots/{lotID}", handlers.DeleteLotHandler)
		r.Get("/lots", handlers.GetLotsHandler)
		r.Put("/lots/{id}/buy", handlers.BuyLotHandler)

	})

	err := http.ListenAndServe(":8000", r)
	if err != nil {
		log.Fatal(err)
	}
}
