package handlers

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"strconv"
	"strings"
	"tfs-go-a/internal/lot"
	"tfs-go-a/internal/session"
	"tfs-go-a/internal/user"
	"time"

	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/postgres"
)

func SignUpHandler(w http.ResponseWriter, r *http.Request) {
	log.Print("----------------SignUpHandler----------------")
	var usr user.User

	err := json.NewDecoder(r.Body).Decode(&usr)
	if err != nil {
		fmt.Fprintf(w, "SignUpHandler: Error decoding body: %s \n", err)
	}
	defer r.Body.Close()
	if usr.Email == "" || usr.Password == "" {
		fmt.Fprintf(w, "You must provide both email and password \n")
		return
	}

	db, err := gorm.Open("postgres", "user=andrey password=andrey dbname=fintech sslmode=disable")
	if err != nil {
		log.Fatalf("can`t connect to database: ", err.Error())
	}
	defer db.Close()

	db.Create(&usr)
	if err != nil {
		log.Fatalf("can`t create user: ", err.Error())
	}
	log.Printf("user %s was created successfuly, id = %d", usr.Email, usr.Id)
	w.Write([]byte(fmt.Sprintf("user %d was successfuly created \n", usr.Id)))
	// if err != nil {
	// 	log.Fatalf("can`t write ", err.Error())
	// }
}

func UpdateUser(w http.ResponseWriter, r *http.Request) {
	log.Print("----------------UpdateUserHandler----------------")

	token := r.Header.Get("Authorization")
	userID := strings.Split(r.URL.Path, "/")[4] //strings.TrimPrefix(r.URL.Path, "users/")
	var reqSession session.Session
	var usr user.User

	db, err := gorm.Open("postgres", "user=andrey password=andrey dbname=fintech sslmode=disable")
	if err != nil {
		log.Fatalf("CreateLotHandler(): can`t connect to database: ", err.Error())
	}
	defer db.Close()

	db.First(&reqSession, "user_id = ?", userID)
	db.First(&usr, "id = ?", userID)
	if reqSession.SessionID == token && time.Time.Before(time.Now(), reqSession.ValidUntil) {
		db.Model(&usr).Updates(&usr)
		log.Printf("user was successfuly updated, id = %s", userID)
	} else {
		fmt.Fprint(w, "failed to update")
	}

}

func SignInHandler(w http.ResponseWriter, r *http.Request) {
	log.Print("----------------SignInHandler----------------")
	var reqData user.User
	var usr user.User

	err := json.NewDecoder(r.Body).Decode(&reqData)
	if err != nil {
		fmt.Fprintf(w, "SignInHandler: Error decoding body: %s \n", err)
	}
	defer r.Body.Close()
	if reqData.Email == "" || reqData.Password == "" {
		fmt.Fprintf(w, "You must provide both email and password \n")
		return
	}

	db, err := gorm.Open("postgres", "user=andrey password=andrey dbname=fintech sslmode=disable")
	if err != nil {
		log.Fatalf("can`t connect to database: ", err.Error())
	}
	defer db.Close()

	db.First(&usr, "Email = ?", reqData.Email)
	if usr.Password == reqData.Password {

		s := session.CreateSession(usr.Id)
		fmt.Fprint(w, "logged\n")
		fmt.Fprint(w, s)

	} else {
		fmt.Fprintf(w, "not logged")
	}
}

func DeleteUserHandler(w http.ResponseWriter, req *http.Request) {
	fmt.Fprintf(w, "this handler deletes user, not implemented")
}

func CreateLotHandler(w http.ResponseWriter, r *http.Request) {
	log.Print("----------------CreateLotHandler----------------")
	token := r.Header.Get("Authorization")
	var reqSession session.Session
	var reqLot lot.Lot

	db, err := gorm.Open("postgres", "user=andrey password=andrey dbname=fintech sslmode=disable")
	if err != nil {
		log.Fatalf("CreateLotHandler(): can`t connect to database: ", err.Error())
	}
	defer db.Close()

	db.First(&reqSession, "session_id = ?", token)
	if reqSession.SessionID == token && time.Time.Before(time.Now(), reqSession.ValidUntil) {
		CreatedAt := time.Now()
		UpdatedAt := CreatedAt
		userID := reqSession.UserID

		err = json.NewDecoder(r.Body).Decode(&reqLot)
		if err != nil {
			fmt.Fprintf(w, "CreateLotHandler(): Error decoding body: %s \n", err)
		}
		defer r.Body.Close()
		reqLot.CreatedAt = CreatedAt
		reqLot.CreatorID = userID
		reqLot.UpdatedAt = UpdatedAt
		reqLot.EndAt = CreatedAt.Add(time.Hour * 2)
		db.Create(&reqLot)
		fmt.Fprint(w, "Lot created successfuly\n")
		w.WriteHeader(201)
	} else {
		fmt.Fprint(w, "Authorization failed\n")
	}
}

func UpdateLotHandler(w http.ResponseWriter, r *http.Request) {
	log.Print("----------------UpdateLotHandler----------------")
	var reqSession session.Session
	//var usr user.User
	var UpdatedLot lot.Lot

	token := r.Header.Get("Authorization")
	userID := strings.Split(r.URL.Path, "/")[4]
	lotID := strings.Split(r.URL.Path, "/")[6]
	// IntuserID, err := strconv.ParseInt(userID, 10, 64)
	// if err != nil {
	// 	log.Fatalf("UpdateLotHandler(): can`t connect to database: ", err.Error())
	// }
	IntlotID, err := strconv.ParseInt(lotID, 10, 64)
	if err != nil {
		log.Fatalf("UpdateLotHandler(): can`t connect to database: ", err.Error())
	}

	db, err := gorm.Open("postgres", "user=andrey password=andrey dbname=fintech sslmode=disable")
	if err != nil {
		log.Fatalf("UpdateLotHandler(): can`t connect to database: ", err.Error())
	}
	defer db.Close()

	db.First(&reqSession, "user_id = ?", userID)
	//db.First(&usr, "id = ?", userID)
	if reqSession.SessionID == token && time.Time.Before(time.Now(), reqSession.ValidUntil) {
		err = json.NewDecoder(r.Body).Decode(&UpdatedLot)
		if err != nil {
			fmt.Fprintf(w, "UpdateLotHandler(): Error decoding body: %s \n", err)
		}
		defer r.Body.Close()
		UpdatedLot.ID = IntlotID
		UpdatedLot.CreatorID = reqSession.UserID //IntuserID
		UpdatedLot.UpdatedAt = time.Now()
		db.Model(&UpdatedLot).Updates(&UpdatedLot)
		fmt.Fprint(w, "lot successfuly updated")
		w.WriteHeader(200)
	} else {
		fmt.Fprint(w, "failed to update")
	}
}

func DeleteLotHandler(w http.ResponseWriter, r *http.Request) {
	log.Print("----------------DeleteLotHandler----------------")
	var reqSession session.Session
	var deletedLot lot.Lot

	token := r.Header.Get("Authorization")
	lotID := strings.Split(r.URL.Path, "/")[6]
	IntlotID, err := strconv.ParseInt(lotID, 10, 64)
	if err != nil {
	}

	db, err := gorm.Open("postgres", "user=andrey password=andrey dbname=fintech sslmode=disable")
	if err != nil {
		log.Fatalf("UpdateLotHandler(): can`t connect to database: ", err.Error())
	}
	defer db.Close()

	db.First(&reqSession, "session_id = ?", token)
	if reqSession.SessionID == token && time.Time.Before(time.Now(), reqSession.ValidUntil) {
		deletedLot.ID = IntlotID
		deletedLot.IsDeleted = 1 //deletedLot.deleted_at = time.Now()
		deletedLot.UpdatedAt = time.Now()

		db.Model(&deletedLot).Updates(&deletedLot) //db.Where("age = ?", 20).Delete(&User{})
		w.WriteHeader(200)
		//db.Delete(&deletedLot, "id = ?", IntlotID)
	} else {
		fmt.Fprint(w, "failed to update")
	}
}

func UserLotsHandler(w http.ResponseWriter, r *http.Request) {
	log.Print("----------------UserLotsHandler----------------")
	var reqSession session.Session
	var reqLots []lot.Lot

	token := r.Header.Get("Authorization")
	userID := strings.Split(r.URL.Path, "/")[4]
	if token == "" {
		fmt.Fprint(w, "failed to get user lots\n")
		return
	}

	db, err := gorm.Open("postgres", "user=andrey password=andrey dbname=fintech sslmode=disable")
	if err != nil {
		log.Fatalf("UpdateLotHandler(): can`t connect to database: ", err.Error())
	}
	defer db.Close()
	db.First(&reqSession, "user_id = ?", userID)

	if reqSession.SessionID == token && time.Time.Before(time.Now(), reqSession.ValidUntil) {
		db.Where("creator_id = ?", userID).Find(&reqLots)
		resp, err := json.Marshal(reqLots)
		if err != nil {
			log.Fatalf("UserLotsHandler(): can`t marshall to json: ", err.Error())
		}
		w.WriteHeader(200)
		w.Header().Set("Content-Type", "application/json")
		w.Write(resp)
	} else {
		fmt.Fprint(w, "failed to get user lots: Bad authorization token\n")
	}
}

func GetLotsHandler(w http.ResponseWriter, r *http.Request) {
	log.Print("----------------GetLotsHandler----------------")
	var reqSession session.Session
	var reqLots []lot.Lot

	status := r.URL.Query()["active"]

	token := r.Header.Get("Authorization")
	if token == "" {
		fmt.Fprint(w, "failed to get user lots\n")
		return
	}
	var resp []byte

	db, err := gorm.Open("postgres", "user=andrey password=andrey dbname=fintech sslmode=disable")
	if err != nil {
		log.Fatalf("UpdateLotHandler(): can`t connect to database: ", err.Error())
	}
	defer db.Close()
	db.First(&reqSession, "session_id = ?", token)
	if time.Time.Before(time.Now(), reqSession.ValidUntil) {
		if status[0] == "true" {
			db.Where("is_deleted = ? AND active = ?", 0, true).Find(&reqLots)
			resp, err = json.Marshal(reqLots)
			if err != nil {
				log.Fatalf("UserLotsHandler(): can`t marshall to json: ", err.Error())
			}
		} else {
			db.Where("is_deleted = ?", 0).Find(&reqLots)
			resp, err = json.Marshal(reqLots)
			if err != nil {
				log.Fatalf("UserLotsHandler(): can`t marshall to json: ", err.Error())
			}
		}
		w.WriteHeader(200)
		w.Header().Set("Content-Type", "application/json")
		w.Write(resp)
	} else {
		fmt.Fprint(w, "failed to get lots: Bad authorization token\n")
	}
}

func GetLotHandler(w http.ResponseWriter, r *http.Request) {
	log.Print("----------------GetLotHandler----------------")
	var reqSession session.Session
	var reqLot lot.Lot

	lotID := strings.Split(r.URL.Path, "/")[4]
	token := r.Header.Get("Authorization")
	if token == "" {
		fmt.Fprint(w, "failed to get user lots\n")
		return
	}

	db, err := gorm.Open("postgres", "user=andrey password=andrey dbname=fintech sslmode=disable")
	if err != nil {
		log.Fatalf("UpdateLotHandler(): can`t connect to database: ", err.Error())
	}
	defer db.Close()
	db.First(&reqSession, "session_id = ?", token)
	if time.Time.Before(time.Now(), reqSession.ValidUntil) {
		db.Where("ID = ?", lotID).Find(&reqLot)
		resp, err := json.Marshal(reqLot)
		if err != nil {
			log.Fatalf("UserLotsHandler(): can`t marshall to json: ", err.Error())
		}
		w.WriteHeader(200)
		w.Header().Set("Content-Type", "application/json")
		w.Write(resp)
	} else {
		fmt.Fprint(w, "failed to get lots: Bad authorization token\n")
	}
}

func BuyLotHandler(w http.ResponseWriter, r *http.Request) {
	log.Print("----------------BuyLotHandler----------------")
	var reqSession session.Session
	var reqLot lot.Lot
	var byer user.User

	type price struct {
		userPrice float64 `json: price`
	}
	var reqPrice price

	lotID := strings.Split(r.URL.Path, "/")[2]
	token := r.Header.Get("Authorization")
	if token == "" {
		fmt.Fprint(w, "failed to get user lots\n")
		return
	}

	db, err := gorm.Open("postgres", "user=andrey password=andrey dbname=fintech sslmode=disable")
	if err != nil {
		log.Fatalf("UpdateLotHandler(): can`t connect to database: ", err.Error())
	}
	if time.Time.Before(time.Now(), reqSession.ValidUntil) {
		db.First(&reqSession, "session_id = ?", token)
		db.First(&byer, "id = ?", reqSession.UserID)
		err = json.NewDecoder(r.Body).Decode(&reqPrice)
		if err != nil {
			fmt.Fprintf(w, "Error decoding body: %s \n", err)
		}
		db.First(&reqLot, "id = ?", lotID)
		if reqLot.Active == true && reqPrice.userPrice%reqLot.PriceStep == 0 &&
			reqPrice.userPrice > reqLot.MinPrice {
			reqLot.Byer = byer
			reqLot.MinPrice = reqPrice.userPrice
			db.Save(&reqLot)

		}
	}

}
