package pdb

import (
	"database/sql"
	"log"

	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/postgres"
)

var Db *sql.DB

// func init() {
// 	var err error
// 	Db, err = sql.Open("postgres", "postgres://andrey:abc123@localhost/a")
// 	if err != nil {
// 		log.Fatal("can't connect to db: %s", err)
// 	}
//
// 	if err = Db.Ping(); err != nil {
// 		log.Fatal("can't ping db: %s", err)
// 	}
// 	defer Db.Close()
// }

func main() {
	dsn := "postgres://andrey:abc123@localhost/a?sslmode=disable&fallback_application_name=fintech-app"
	db, err := gorm.Open("postgres", dsn)
	if err != nil {
		log.Fatalf("can't connect to db: %s", err)
	}
	defer db.Close()
}
