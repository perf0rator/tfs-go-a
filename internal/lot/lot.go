package lot

import (
	"log"
	"tfs-go-a/internal/user"
	"time"

	"github.com/jinzhu/gorm"
)

type Lot struct {
	ID          int64     `gorm:"primary_key"`
	CreatorID   int64     `gorm:"column:creator_id"`
	Creator     user.User `gorm:"ForeignKey:CreatorID;AssociationForeignKey:ID"`
	Byer        user.User
	Title       string
	Description string
	MinPrice    float64
	PriceStep   float64
	CreatedAt   time.Time
	UpdatedAt   time.Time
	EndAt       time.Time
	Active      bool
	IsDeleted   int
}

func InitialLotMigration() {
	db, err := gorm.Open("postgres", "user=andrey password=andrey dbname=fintech sslmode=disable")
	if err != nil {
		log.Fatalf("can`t connect to database: ", err.Error())
	}
	defer db.Close()

	db.AutoMigrate(&Lot{})

}
