package user

import (
	"log"
	"math/rand"
	"time"

	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/postgres"
)

var db *gorm.DB
var err error

type User struct {
	Id         int64     `gorm:"primary_key"`
	First_name string    `json:"first_name"`
	Last_name  string    `json:"last_name"`
	Birthday   time.Time `json:"birthday"`
	Email      string    `json:"email"`
	Password   string    `json:"password"`
	Created_at time.Time `json:"created_at"`
	Updated_at time.Time `json:"updated_at"`
}

func InitialUserMigration() {
	db, err := gorm.Open("postgres", "user=andrey password=andrey dbname=fintech sslmode=disable")
	if err != nil {
		log.Fatalf("can`t connect to database: ", err.Error())
	}
	defer db.Close()

	db.AutoMigrate(&User{})

}

func idGenerartor() int64 {
	r := rand.Intn(1000)
	ts := time.Now().Unix()
	id := ts + int64(r)
	return id
}

type Users struct {
	Users []User
}
