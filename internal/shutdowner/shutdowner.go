package shutdowner

import (
	"log"
	"tfs-go-a/internal/lot"
	"time"

	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/postgres"
)

func main() {
	var Lot lot.Lot

	db, err := gorm.Open("postgres", "user=andrey password=andrey dbname=fintech sslmode=disable")
	if err != nil {
		log.Fatalf("UpdateLotHandler(): can`t connect to database: ", err.Error())
	}
	defer db.Close()

	for {
		db.Where("EndAt < ?", time.Now()).Find(&Lot)
		db.Delete(&Lot)
		time.Sleep(1 * time.Second)
	}

}
