package session

import (
	"fmt"
	"log"
	"math/rand"
	"time"

	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/postgres"
)

type Session struct {
	SessionID  string    //Идентификтаор сессии (токен)
	UserID     int64     //Идентификатор пользователя
	CreatedAt  time.Time //Дата создания токена
	ValidUntil time.Time //Дата, до которой сессия считается действительной
}

func InitialSessionMigration() {
	db, err := gorm.Open("postgres", "user=andrey password=andrey dbname=fintech sslmode=disable")
	if err != nil {
		log.Fatalf("can`t connect to database: ", err.Error())
	}
	defer db.Close()

	db.AutoMigrate(&Session{})

}

func randomString(l int) string {
	rand.Seed(time.Now().UTC().UnixNano())
	var letterRunes = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890"
	bytes := make([]byte, l)
	for i := 0; i < l; i++ {
		bytes[i] = letterRunes[rand.Intn(len(letterRunes))]
	}
	return string(bytes)
}

func CreateSession(userID int64) Session {
	db, err := gorm.Open("postgres", "user=andrey password=andrey dbname=fintech sslmode=disable")
	if err != nil {
		log.Fatalf("CreateSession(): can`t connect to database: ", err.Error())
	}
	defer db.Close()

	var toDelete Session

	db.Delete(&toDelete, "user_id = ?", userID)
	sessionID := randomString(15)
	createdAt := time.Now() //.UnixNano()
	validUntil := createdAt.Add(time.Hour * 3)
	session := Session{sessionID, userID, createdAt, validUntil}

	fmt.Println(sessionID)

	db.Create(&session)
	return session
}

func CreateLot() {

}
